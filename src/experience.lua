local bn_xp_hud = dofile(minetest.get_modpath("bn_xp") .. "/src/hud.lua");
local experience = {
    init_new_player = function(player)
        bn_xp_utilities.set_experience(player, 0)
    end,
    reset_player = function(player)
        bn_xp_utilities.add_experience(player, -1 * (bn_xp_utilities.get_experience(player) * 0.90))
        minetest.chat_send_player(player:get_player_name(), "You died, lost 10% of your experience.")
    end,
    on_player_dig = function(position, oldnode, player)
        local node_name = oldnode.name
        local give_xp = true
        local give_xp_amount = 1

        local stone_with = "default:stone_with_"
        if node_name == "default:stone" then
            give_xp_amount = 1
        elseif node_name == stone_with .. "coal" then
            give_xp_amount = 2
        elseif node_name == stone_with .. "copper" then
            give_xp_amount = 5
        elseif node_name == stone_with .. "diamond" then
            give_xp_amount = 8
        elseif node_name == stone_with .. "gold" then
            give_xp_amount = 8
        end

        if give_xp then
            bn_xp_utilities.add_experience(player, give_xp_amount)
            bn_xp_hud.update_hud(player)
        end
    end,
    -- itemstack = output
    -- player = player who crafted
    -- old_craft_grid = recipe
    -- craft_inv  = inventory with crafting grid
    -- returns nil to not modify the stack or an itemstack to modify it
    -- DOES NOT WORK WITH FURNACE RECIPES
    on_player_craft = function(itemstack, player, old_craft_grid, craft_inv)
        -- TODO this should only give xp for crafting tools
        local give_xp_amount = 1
        bn_xp_utilities.add_experience(player, give_xp_amount)
        bn_xp_hud.update_hud(player)
    end
}

return experience;