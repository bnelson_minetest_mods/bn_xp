local utilities = {
    reward = "default:mese"
}

local ATTR_XP = "bn_xp_experience"
local ATTR_LVL = "bn_xp_level"

local function calculate_level(experience)
    experience = tonumber(experience)
    return math.floor(experience / 100) + 1
end

--
local function give_reward(player, level)
    minetest.chat_send_player(player:get_player_name(), "Level Up!! Level: " .. level)
    minetest.env:add_item(player:get_pos(), utilities.reward .. " " .. math.floor(level / 5) + 1)
end

-- returns attribute as number, returns default if value is missing after setting the attribute to the player
local function get_number_attr(player, attribute, default)
    local value = default
    if player ~= nil then
        local tmpValue = player:get_attribute(attribute)
        if tmpValue == nil then
            player:set_attribute(attribute, value)
        else
            value = tmpValue
        end
    end
    return tonumber(value)
end

-- sets an attribute to the player
local function set_attribute(player, attribute, value)
    if player ~= nil then
        player:set_attribute(attribute, value)
    end
end

-- gets the experience for the player
local function get_experience(player)
    return get_number_attr(player, ATTR_XP, 0)
end

-- sets the level for the player
local function set_level(player, experience)
    set_attribute(player, ATTR_LVL, experience)
end

-- gets the level for the player
local function get_level(player)
    return utilities.get_number_attr(player, ATTR_LVL, 0)
end

-- sets the experience for the player
local function set_experience(player, experience)
    set_attribute(player, ATTR_XP, experience)
    local level = calculate_level(experience)
    set_level(player, level)
end

-- sets the experience for the player
local function add_experience(player, experience)
    local currentXp = get_experience(player)
    set_attribute(player, ATTR_XP, currentXp + experience)
    local currentLevel = calculate_level(currentXp)
    local newLevel = calculate_level(currentXp + experience)
    if currentLevel ~= newLevel then
        set_level(player, newLevel)
        give_reward(player, newLevel)
    end
end

utilities.get_number_attr = get_number_attr
utilities.set_attribute = set_attribute
utilities.get_experience = get_experience
utilities.calculate_level = calculate_level
utilities.set_experience = set_experience
utilities.add_experience = add_experience
utilities.get_level = get_level

return utilities;