-- the hud
local huds = {}
local bn_xp_hud = {}

--
-- Initializes hud for joined player
--
local function init_hud(player)
    if player == nil then
        return
    end
    local player_name = player:get_player_name()
    local level = bn_xp_utilities.get_level(player)
    local pos_x = bn_xp_utilities.get_number_attr(player, "better_xp_hud_pos_x", 1)
    local pos_y = bn_xp_utilities.get_number_attr(player, "better_xp_hud_pos_y", -1)
    local off_x = bn_xp_utilities.get_number_attr(player, "better_xp_hud_off_x", 0)
    local off_y = bn_xp_utilities.get_number_attr(player, "better_xp_hud_off_y", 0)
    local player_hud = player:hud_add({
        hud_elem_type = "text",
        position = { x = pos_x, y = pos_y },
        offset = { x = off_x, y = off_y },
        scale = { x = 1, y = 1 },
        alignment = { x = 1, y = -1 },
        text = tostring(level),
        number = 0xFFFFFF
    })
    huds[player_name] = player_hud
end

--
-- Uninitializes hud for leaving player
--
local function remove_hud(player)
    if player == nil then
        return nil
    end
    huds[player:get_player_name()] = nil
end

--
-- Updates the hud with player values
--
local function update_hud(player)
    if player == nil then
        return nil
    end
    local playerHud = huds[player:get_player_name()]
    if playerHud ~= nil then
        player:hud_change(playerHud, "text", bn_xp_utilities.get_level(player))
    end
end

--
-- Updates the huds position
--
local function update_hud_position(player)
    if player == nil then
        return nil
    end

    local playerHud = huds[player:get_player_name()]
    if playerHud ~= nil then
        local pos_x = bn_xp_utilities.get_number_attr(player, "better_xp_hud_pos_x", 1)
        local pos_y = bn_xp_utilities.get_number_attr(player, "better_xp_hud_pos_y", 0)
        local off_x = bn_xp_utilities.get_number_attr(player, "better_xp_hud_off_x", -40)
        local off_y = bn_xp_utilities.get_number_attr(player, "better_xp_hud_off_y", 40)
        player:hud_change(playerHud, "position", { x = pos_x, y = pos_y })
        player:hud_change(playerHud, "offset", { x = off_x, y = off_y })
    end
end

--
-- Sets HUD defaults
--
local function set_hud_defaults(player)
    bn_xp_utilities.set_attribute(player, "better_xp_hud_pos_x", 1)
    bn_xp_utilities.set_attribute(player, "better_xp_hud_pos_y", 0)
    bn_xp_utilities.set_attribute(player, "better_xp_hud_off_x", -40)
    bn_xp_utilities.set_attribute(player, "better_xp_hud_off_y", 40)
    update_hud_position(player)
end

--
-- Helper to update hud position value and set the attr on the player to remember
--
local function hud_update_helper(playerName, hudAttribute, value)
    local player = minetest.get_player_by_name(playerName)
    bn_xp_utilities.set_attribute(player, hudAttribute, tonumber(value))
    update_hud_position(player)
end


minetest.register_chatcommand("xp_hud_reset", {
    func = function(name)
        local player = minetest.get_player_by_name(name)
        set_hud_defaults(player)
        return true, "Updated Hud position"
    end
})
minetest.register_chatcommand("xp_hud_pos_x", {
    func = function(name, param)
        hud_update_helper(name, "better_xp_hud_pos_x", param)
        return true, "Updated Hud position"
    end
})
minetest.register_chatcommand("xp_hud_pos_y", {
    func = function(name, param)
        hud_update_helper(name, "better_xp_hud_pos_y", param)
        return true, "Updated Hud position"
    end
})
minetest.register_chatcommand("xp_hud_off_x", {
    func = function(name, param)
        hud_update_helper(name, "better_xp_hud_off_x", param)
        return true, "Updated Hud position"
    end
})
minetest.register_chatcommand("xp_hud_off_y", {
    func = function(name, param)
        hud_update_helper(name, "better_xp_hud_off_y", param)
        return true, "Updated Hud position"
    end
})
minetest.register_on_joinplayer(init_hud)
minetest.register_on_leaveplayer(remove_hud)

bn_xp_hud.update_hud = update_hud
return bn_xp_hud