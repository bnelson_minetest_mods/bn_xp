bn_xp_utilities = dofile(minetest.get_modpath("bn_xp") .. "/src/utilities.lua");
local xp_experience = dofile(minetest.get_modpath("bn_xp") .. "/src/experience.lua");

-- Register functins on player
minetest.register_on_newplayer(xp_experience.init_new_player)
minetest.register_on_dieplayer(xp_experience.reset_player)
minetest.register_on_dignode(xp_experience.on_player_dig)
minetest.register_on_craft(xp_experience.on_player_craft)
minetest.register_privilege("xp_admin", "Player can manage xp points and level.")
minetest.register_chatcommand("xp", {
    func = function(name)
        local player = minetest.get_player_by_name(name)
        if not player then
            return false, "Player not found"
        end
        return true, "Current Xp: " .. bn_xp_utilities.get_experience(player) .. " Current Level: " .. bn_xp_utilities.get_level(player)
    end
})
minetest.register_chatcommand("xp_reset", {
    privs = { xp_admin = true },
    func = function(name)
        local player = minetest.get_player_by_name(name)
        if not player then
            return false, "Player not found"
        end
        bn_xp_utilities.set_experience(player, 0)
        return true, "reset xp"
    end
})
minetest.register_chatcommand("xp_giveme", {
    privs = { xp_admin = true },
    func = function(name, param)
        local player = minetest.get_player_by_name(name)
        if not player then
            return false, "Player not found"
        end
        bn_xp_utilities.add_experience(player, param)
        return true, "added xp"
    end
})
minetest.register_chatcommand("xp_set_reward", {
    privs = { xp_admin = true },
    func = function(name, param)
        bn_xp_utilities.reward = param
        return true, "reward is now set to '" .. param .. "'"
    end
})